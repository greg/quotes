# The Art of Doing Science and Engineering | Learning to Learn

Education is what, when, and why to do things.
Training is how to do it.

In science, if you know what you are doing, you should not be doing it.
In engineering, if you do not know what you are doing, you should not be doing it.
