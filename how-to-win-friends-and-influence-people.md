# How to Win Friends and Influence People

## Fundamental Techniques in Handling People

1. Don't criticize, condemn, or complain.
1. Give honest and sincere appreciation.
1. Arouse in the other person an eager want.

## Six Ways to Make People Like You

1. Become genuinely interested in other people.
1. Smile.
1. Remember that a person's name is, to that person, the sweetest and most important sound in any language.
1. Be a good listener. Encourage others to talk about themselves.
1. Talk in terms of the other person's interest.
1. Make the other person feel important – and do it sincerely.

## Twelve Ways to Win People to Your Way of Thinking

1. The only way to get the best of an argument is to avoid it. 
1. Show respect for the other person's opinions. Never say "You're wrong."
1. If you're wrong, admit it quickly and emphatically.
1. Begin in a friendly way.
1. Start with questions to which the other person will answer yes.
1. Let the other person do a great deal of the talking.
1. Let the other person feel the idea is his or hers.
1. Try honestly to see things from the other person's point of view.
1. Be sympathetic with the other person's ideas and desires.
1. Appeal to the nobler motives.
1. Dramatize your ideas.
1. Throw down a challenge.